﻿using Softversko2.EStore.Model.Entities.Brands;
using Softversko2.EStore.Model.Entities.Categories;
using Softversko2.EStore.Model.Entities.Delovi;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softversko2.EStore.Repository.Delovi
{
    public class DeoRepository : IDeoRepository
    {
        private string _connectionString;

        public DeoRepository()
        {
            _connectionString =
                ConfigurationManager.ConnectionStrings["EStoreConnectionString"].ConnectionString;
        }

        public List<Deo> ReadAll()
        {
            List<Deo> delovi = new List<Deo>();
            string queryStringDeo =
                "SELECT d.DeoId, d.Ime, d.Opis, d.Velicina, d.Cena, d.ImageData, d.ImageMimeType, " +
                "       b.BrandId, b.Name " +
                "FROM dbo.T_DEO d, dbo.T_BRAND b" +
                "WHERE d.BrandId = b.BrandId";
            string queryStringDeoImages =
            "SELECT DeoId, ImageData, ImageMimeType FROM dbo.T_DEO_IMAGE WHERE DeoId = @DeoId";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = queryStringDeo;
                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    Deo deo;
                    while (reader.Read())
                    {
                        deo = new Deo();
                        deo.DeoId = Int32.Parse(reader[0].ToString());
                        deo.Ime = reader[1].ToString();
                        deo.Opis = reader[2].ToString();
                        deo.Velicina = reader[3].ToString();
                        deo.Cena = Decimal.Parse(reader[4].ToString());

                        deo.DeoImage = new DeoImage();
                        deo.DeoImage.ImageData = string.IsNullOrEmpty(reader[5].ToString()) ? null : (byte[])reader[5];
                        deo.DeoImage.ImageMimeType = reader[6].ToString();


                        deo.Brand = new Brand();
                        deo.Brand.BrandId = Int32.Parse(reader[7].ToString());
                        deo.Brand.Name = reader[8].ToString();

                        delovi.Add(deo);
                    }
                }

                foreach (Deo deo in delovi)
                {
                    deo.DeoImages = new List<DeoImage>();
                    SqlCommand commandDeoImage = connection.CreateCommand();
                    commandDeoImage.CommandText = queryStringDeoImages;
                    commandDeoImage.Parameters.Add(new SqlParameter("@DeoId", deo.DeoId));
                    using (SqlDataReader reader = commandDeoImage.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DeoImage deoImage = new DeoImage();
                            deoImage.ImageData = (byte[])reader[1];
                            deoImage.ImageMimeType = reader[2].ToString();
                            deo.DeoImages.Add(deoImage);
                        }
                    }
                }
            }
            return delovi;
        }

        public void Update(Deo entity)
        {
            string updateDeoSql =
                "UPDATE T_DEO SET Ime = @Ime, Opis = @Opis," +
                "Velicina = @Velicina, Cena = @Cena,  BrandId = @BrandId, ImageData = @ImageData, ImageMimeType = @ImageMimeType " +
                "WHERE DeoId = @DeoId;";
            string deleteDeoImageSql =
                "DELETE T_DEO_IMAGE WHERE DeoId = @DeoId;";
            string insertDeoImageSql =
                "INSERT INTO T_DEO_IMAGE (DeoId, ImageData, ImageMimeType) " +
                "VALUES (@DeoId, @ImageData, @ImageMimeType)";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand commandDeo = connection.CreateCommand();
                commandDeo.CommandText = updateDeoSql;
                commandDeo.Parameters.Add(new SqlParameter("@DeoId", entity.DeoId));
                commandDeo.Parameters.Add(new SqlParameter("@Ime", entity.Ime));
                commandDeo.Parameters.Add(new SqlParameter("@Opis", entity.Opis));
                commandDeo.Parameters.Add(new SqlParameter("@Velicina", entity.Velicina));
                commandDeo.Parameters.Add(new SqlParameter("@Cena", entity.Cena));
                commandDeo.Parameters.Add(new SqlParameter("@BrandId", entity.Brand.BrandId));
                commandDeo.Parameters.Add(new SqlParameter("@ImageData", entity.DeoImage.ImageData));
                commandDeo.Parameters.Add(new SqlParameter("@ImageMimeType", entity.DeoImage.ImageMimeType));
                connection.Open();
                commandDeo.ExecuteNonQuery();

                SqlCommand commandDeleteDeoImages = connection.CreateCommand();
                commandDeleteDeoImages.CommandText = deleteDeoImageSql;
                commandDeleteDeoImages.Parameters.Add(new SqlParameter("@DeoId", entity.DeoId));
                commandDeleteDeoImages.ExecuteNonQuery();

                SqlCommand commandInsertDeoImages = connection.CreateCommand();
                commandInsertDeoImages.CommandText = insertDeoImageSql;
                foreach (DeoImage deoImage in entity.DeoImages)
                {
                    commandInsertDeoImages.Parameters.Clear();
                    commandInsertDeoImages.Parameters.Add(new SqlParameter("@DeoId", entity.DeoId));
                    commandInsertDeoImages.Parameters.Add(new SqlParameter("@ImageData", deoImage.ImageData));
                    commandInsertDeoImages.Parameters.Add(new SqlParameter("@ImageMimeType", deoImage.ImageMimeType));
                    commandInsertDeoImages.ExecuteNonQuery();
                }
            }
        }

        public void Create(Deo entity)
        {
            string insertDeoSql =
                "INSERT INTO T_DEO (Ime, Opis, Velicina, Cena, BrandId, ImageData, ImageMimeType) " +
                "VALUES (@Ime, @Opis,  @Velicina, @Cena, @BrandId, @ImageData, @ImageMimeType); " +
                "SELECT CAST(SCOPE_IDENTITY() AS INT);";
            string insertdeoImageSql =
                "INSERT INTO T_DEO_IMAGE (DeoId, ImageData, ImageMimeType) " +
                "VALUES (@DeoId, @ImageData, @ImageMimeType)";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand commandDeo = connection.CreateCommand();
                commandDeo.CommandText = insertDeoSql;
                commandDeo.Parameters.Add(new SqlParameter("@Ime", entity.Ime));
                commandDeo.Parameters.Add(new SqlParameter("@Opis", entity.Opis));
                commandDeo.Parameters.Add(new SqlParameter("@Velicina", entity.Velicina));
                commandDeo.Parameters.Add(new SqlParameter("@Cena", entity.Cena));
                commandDeo.Parameters.Add(new SqlParameter("@BrandId", entity.Brand.BrandId));
                commandDeo.Parameters.Add(new SqlParameter("@ImageData", entity.DeoImage.ImageData));
                commandDeo.Parameters.Add(new SqlParameter("@ImageMimeType", entity.DeoImage.ImageMimeType));
                connection.Open();
                int insertedId = (int)commandDeo.ExecuteScalar();

                SqlCommand commandDeoImage = connection.CreateCommand();
                commandDeoImage.CommandText = insertdeoImageSql;
                foreach (DeoImage deoImage in entity.DeoImages)
                {
                    commandDeoImage.Parameters.Clear();
                    commandDeoImage.Parameters.Add(new SqlParameter("@DeoId", insertedId));
                    commandDeoImage.Parameters.Add(new SqlParameter("@ImageData", deoImage.ImageData));
                    commandDeoImage.Parameters.Add(new SqlParameter("@ImageMimeType", deoImage.ImageMimeType));
                    commandDeoImage.ExecuteNonQuery();
                }
            }
        }

        public void Delete(int id)
        {
            string deleteSql = "DELETE T_DEO WHERE DeoId = @DeoId;" +
                "DELETE T_DEO_IMAGE WHERE DeoId = @DeoId;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = deleteSql;
                command.Parameters.Add(new SqlParameter("@DeoId", id));
                connection.Open();
                command.ExecuteNonQuery();
            }
        }
    }
}
