﻿using Softversko2.EStore.AppService.Abstractions.Brands;
using Softversko2.EStore.AppService.Abstractions.Delovi;
using Softversko2.EStore.AppService.Messages.Delovi;
using Softversko2.EStore.Model.Entities.Delovi;
using Softversko2.EStore.Web.Areas.Administrations.Models;
using Softversko2.EStore.Web.Mappings.Brands;
using Softversko2.EStore.Web.Mappings.Delovi;
using Softversko2.EStore.Web.Models.Delovi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Softversko2.EStore.Web.Areas.Administrations.Controllers
{
    [Authorize]
    public class DeloviController : Controller
    {
        private IDeoService deoService;
        private IBrandService brandService;
     

        public DeloviController(IDeoService deoService,
            IBrandService brandService)
        {
            this.deoService = deoService;
            this.brandService = brandService;
          
        }

        public ActionResult Index()
        {
            DeloviListPageViewModel model = new DeloviListPageViewModel();
            FindAllDeloviResponse response = deoService.FindAllDelovi();
            if (response.Success)
            {
                model.DeoViewModels = response.Delovi.ConvertToDeoViewModelList();
                model.Success = true;
            }
            else
            {
                model.Success = false;
                model.ErrorMessage = response.Message;
            }

            return View(model);
        }

        public ActionResult Create()
        {
            DeloviSinglePageViewModel model = new DeloviSinglePageViewModel();
            model.DeoViewModel = new DeoViewModel();
            model.BrandViewModels = brandService.FindAllBrands().Brands.ConvertToBrandViewModelList();
            return View("Edit", model);
        }

        public ActionResult Edit(int deoId)
        {
            DeloviSinglePageViewModel model = new DeloviSinglePageViewModel();
            FindAllDeloviResponse response = deoService.FindAllDelovi();
            if (response.Success)
            {
                model.DeoViewModel = response.Delovi.
                    Where(x => x.DeoId == deoId).
                    FirstOrDefault().
                    ConvertToDeoViewModel();
                model.BrandViewModels = brandService.FindAllBrands().Brands.ConvertToBrandViewModelList();
                model.Success = true;
            }
            else
            {
                model.Success = false;
                model.ErrorMessage = response.Message;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(DeloviSinglePageViewModel model, List<HttpPostedFileBase> fileUpload)
        {
            if (model.DeoViewModel.DeoId == 0)
            {
                CreateDeloviRequest request = new CreateDeloviRequest();
                CreateDeloviResponse response = new CreateDeloviResponse();
                request.Ime= model.DeoViewModel.Ime;
                request.Opis = model.DeoViewModel.Opis;
                request.Cena = model.DeoViewModel.Cena;
                request.Velicina = model.DeoViewModel.Velicina;
                request.BrandId = model.DeoViewModel.BrandId;
                request.DeoImages = new List<DeoImage>();

                foreach (HttpPostedFileBase image in fileUpload)
                {
                    if (image != null)
                    {
                        DeoImage deoImeage = new DeoImage();
                        deoImeage.ImageMimeType = image.ContentType;
                        deoImeage.ImageData = new byte[image.ContentLength];
                        image.InputStream.Read(deoImeage.ImageData, 0, image.ContentLength);
                        // First image add for displaying in product list page
                        if (request.DeoImage == null)
                        {
                            request.DeoImage = new DeoImage();
                            request.DeoImage = deoImeage;
                        }
                        // Other images add to list
                        request.DeoImages.Add(deoImeage);
                    }
                }

                response = deoService.CreateDeo(request);
                if (response.Success)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    model.Success = false;
                    model.ErrorMessage = response.Message;
                    return View(model);
                }
            }
            else
            {
                UpdateDeloviRequest request = new UpdateDeloviRequest();
                UpdateDeloviResponse response = new UpdateDeloviResponse();
                request.DeoId = model.DeoViewModel.DeoId;
                request.Ime = model.DeoViewModel.Ime;
                request.Opis = model.DeoViewModel.Opis;
                request.Cena = model.DeoViewModel.Cena;
                request.Velicina = model.DeoViewModel.Velicina;
                request.BrandId = model.DeoViewModel.BrandId;
                request.DeoImages = new List<DeoImage>();

                foreach (HttpPostedFileBase image in fileUpload)
                {
                    if (image != null)
                    {
                        DeoImage deoImeage = new DeoImage();
                        deoImeage.ImageMimeType = image.ContentType;
                        deoImeage.ImageData = new byte[image.ContentLength];
                        image.InputStream.Read(deoImeage.ImageData, 0, image.ContentLength);
                        // First image add for displaying in product list page
                        if (request.DeoImage == null)
                        {
                            request.DeoImage = new DeoImage();
                            request.DeoImage = deoImeage;
                        }
                        // Other images add to list
                        request.DeoImages.Add(deoImeage);
                    }
                }
                response = deoService.UpdateDeo(request);
                if (response.Success)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    model.Success = false;
                    model.ErrorMessage = response.Message;
                    return View(model);
                }
            }
        }

        public ActionResult Delete(int DeoId)
        {
            DeleteDeloviRequest request = new DeleteDeloviRequest();
            DeleteDeloviResponse response = new DeleteDeloviResponse();
            request.DeoId = DeoId;
            response = deoService.DeleteDeo(request);
            if (response.Success)
            {
                return RedirectToAction("Index");
            }
            else
            {
               DeloviListPageViewModel model = new DeloviListPageViewModel();
                model.Success = false;
                model.ErrorMessage = response.Message;
                return View("Index", model);
            }
        }
    }
}