﻿using Softversko2.EStore.Web.Models.Products;
using System.Collections.Generic;

namespace Softversko2.EStore.Web.Areas.Administrations.Models
{
    public class ProductListPageViewModel
    {
        public List<ProductViewModel> ProductViewModels { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
