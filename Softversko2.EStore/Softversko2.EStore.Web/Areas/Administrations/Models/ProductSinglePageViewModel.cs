﻿using Softversko2.EStore.Web.Models.Brands;
using Softversko2.EStore.Web.Models.Categories;
using Softversko2.EStore.Web.Models.Products;
using System.Collections.Generic;

namespace Softversko2.EStore.Web.Areas.Administrations.Models
{
    public class ProductSinglePageViewModel
    {
        public ProductViewModel ProductViewModel { get; set; }
        public List<BrandViewModel> BrandViewModels { get; set; }
        public List<CategoryViewModel> CategoryViewModels { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
