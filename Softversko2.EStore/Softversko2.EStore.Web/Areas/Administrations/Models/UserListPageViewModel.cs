﻿using Softversko2.EStore.Web.Models.Users;
using System.Collections.Generic;

namespace Softversko2.EStore.Web.Areas.Administrations.Models
{
    public class UserListPageViewModel
    {
        public List<UserViewModel> UserViewModels { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
