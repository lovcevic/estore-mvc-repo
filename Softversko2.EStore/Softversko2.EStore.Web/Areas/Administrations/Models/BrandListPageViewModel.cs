﻿using Softversko2.EStore.Web.Models.Brands;
using System.Collections.Generic;

namespace Softversko2.EStore.Web.Areas.Administrations.Models
{
    public class BrandListPageViewModel
    {
        public List<BrandViewModel> BrandViewModels { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
