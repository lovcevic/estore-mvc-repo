﻿using Softversko2.EStore.Web.Models.Brands;

namespace Softversko2.EStore.Web.Areas.Administrations.Models
{
    public class BrandSinglePageViewModel
    {
        public BrandViewModel BrandViewModel { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
