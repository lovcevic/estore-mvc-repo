﻿using Softversko2.EStore.Web.Models.Users;

namespace Softversko2.EStore.Web.Areas.Administrations.Models
{
    public class UserSinglePageViewModel
    {
        public UserViewModel UserViewModel { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
