﻿using Softversko2.EStore.Web.Models.Categories;
using System.Collections.Generic;

namespace Softversko2.EStore.Web.Areas.Administrations.Models
{
    public class CategoryListPageViewModel
    {
        public List<CategoryViewModel> CategoryViewModels { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
