﻿using Softversko2.EStore.Web.Models.Categories;

namespace Softversko2.EStore.Web.Areas.Administrations.Models
{
    public class CategorySinglePageViewModel
    {
        public CategoryViewModel CategoryViewModel { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
