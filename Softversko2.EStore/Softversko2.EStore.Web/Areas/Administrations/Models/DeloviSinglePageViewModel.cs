﻿using Softversko2.EStore.Web.Models.Brands;
using Softversko2.EStore.Web.Models.Delovi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Softversko2.EStore.Web.Areas.Administrations.Models
{
    public class DeloviSinglePageViewModel
    {
        public DeoViewModel DeoViewModel { get; set; }
        public List<BrandViewModel> BrandViewModels { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}