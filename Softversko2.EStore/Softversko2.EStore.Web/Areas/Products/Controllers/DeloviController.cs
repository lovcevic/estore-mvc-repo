﻿using Softversko2.EStore.AppService.Abstractions.Delovi;
using Softversko2.EStore.AppService.Implementations.Delovi;
using Softversko2.EStore.AppService.Messages.Delovi;
using Softversko2.EStore.Web.Areas.Products.Models;
using Softversko2.EStore.Web.Mappings.Delovi;
using Softversko2.EStore.Web.Models.Delovi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Softversko2.EStore.Web.Areas.Products.Controllers
{
    public class DeloviController : Controller
    { 
   private IDeoService deoService;

    public DeloviController(IDeoService deoService)
    {
        this.deoService = deoService;
    }

    // GET: Products/Product
    public ActionResult Index()
    {
        FindAllDeloviResponse response = new FindAllDeloviResponse();
        response = deoService.FindAllDelovi();

        AllDeloviPageViewModel model = new AllDeloviPageViewModel();
        model.DeoViewModels = response.Delovi.ConvertToDeoViewModelList();
        model.Success = response.Success;
        model.ErrorMessage = response.Message;

        return View(model);
    }

    public FileContentResult GetImage(int deoId)
    {
        DeoViewModel deoViewModel = deoService.FindAllDelovi().Delovi.
                        Find(x => x.DeoId == deoId).ConvertToDeoViewModel();
        if (deoViewModel != null)
        {
            return File(deoViewModel.DeoImageViewModel.ImageData, deoViewModel.DeoImageViewModel.ImageMimeType);
        }
        else
        {
            return null;
        }
    }
}
}