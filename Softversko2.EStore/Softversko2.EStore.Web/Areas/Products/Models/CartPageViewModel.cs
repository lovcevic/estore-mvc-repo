﻿using Softversko2.EStore.Model.Entities.Carts;

namespace Softversko2.EStore.Web.Areas.Products.Models
{
    public class CartPageViewModel
    {
        public Cart Cart { get; set; }
        public string ReturnUrl { get; set; }
    }
}
