﻿using Softversko2.EStore.Web.Models.Delovi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Softversko2.EStore.Web.Areas.Products.Models
{
    public class AllDeloviPageViewModel
    {
        public List<DeoViewModel> DeoViewModels { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}