﻿using Softversko2.EStore.Web.Models.Users;
using System.Security.Principal;

namespace Softversko2.EStore.Web.Providers
{
    public class EStorePrincipal : IPrincipal
    {
        public EStorePrincipal(IIdentity identity)
        {
            Identity = identity;
        }

        public IIdentity Identity
        {
            get;
            private set;
        }

        public UserViewModel User { get; set; }

        public bool IsInRole(string role)
        {
            if (User.Role.ToString() == role)
            {
                return true;
            }
            return false;
        }
    }
}
