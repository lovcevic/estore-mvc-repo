﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Softversko2.EStore.Web.Startup))]
namespace Softversko2.EStore.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
