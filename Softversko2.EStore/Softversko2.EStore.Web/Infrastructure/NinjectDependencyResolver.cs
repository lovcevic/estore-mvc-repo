﻿using Ninject;
using Softversko2.EStore.AppService.Abstractions.Brands;
using Softversko2.EStore.AppService.Abstractions.Categories;
using Softversko2.EStore.AppService.Abstractions.Delovi;
using Softversko2.EStore.AppService.Abstractions.Products;
using Softversko2.EStore.AppService.Abstractions.Users;
using Softversko2.EStore.AppService.Implementations.Brands;
using Softversko2.EStore.AppService.Implementations.Categories;
using Softversko2.EStore.AppService.Implementations.Delovi;
using Softversko2.EStore.AppService.Implementations.Products;
using Softversko2.EStore.AppService.Implementations.Users;
using Softversko2.EStore.Model.Entities.Brands;
using Softversko2.EStore.Model.Entities.Categories;
using Softversko2.EStore.Model.Entities.Delovi;
using Softversko2.EStore.Model.Entities.Products;
using Softversko2.EStore.Model.Entities.Users;
using Softversko2.EStore.Repository.Brands;
using Softversko2.EStore.Repository.Categories;
using Softversko2.EStore.Repository.Delovi;
using Softversko2.EStore.Repository.Users;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Softversko2.EStore.Web.Infrastructure
{
    class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver()
        {
            kernel = new StandardKernel();
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            kernel.Bind<IProductRepository>().To<ProductRepository>();
            kernel.Bind<IProductService>().To<ProductService>();

            kernel.Bind<IBrandRepository>().To<BrandRepository>();
            kernel.Bind<IBrandService>().To<BrandService>();

            kernel.Bind<ICategoryRepository>().To<CategoryRepository>();
            kernel.Bind<ICategoryService>().To<CategoryService>();

            kernel.Bind<IUserRepository>().To<UserRepository>();
            kernel.Bind<IUserService>().To<UserService>();


            kernel.Bind<IDeoRepository>().To<DeoRepository>();
            kernel.Bind<IDeoService>().To<DeoService>();

        }
    }
}

