﻿using Softversko2.EStore.Model.Entities.Delovi;
using Softversko2.EStore.Web.Models.Delovi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Softversko2.EStore.Web.Mappings.Delovi
{
    public static class DeoMapper
    {
        public static List<DeoViewModel> ConvertToDeoViewModelList(this List<Deo> delovi)
        {
            List<DeoViewModel> deoViewModels = new List<DeoViewModel>();
            foreach (Deo deo in delovi)
            {
                deoViewModels.Add(deo.ConvertToDeoViewModel());
            }
            return deoViewModels;
        }

        public static DeoViewModel ConvertToDeoViewModel(this Deo deo)
        {
            DeoViewModel deoViewModel = new DeoViewModel();
            deoViewModel.DeoId = deo.DeoId;
            deoViewModel.Ime = deo.Ime;
            deoViewModel.Opis = deo.Opis;
            deoViewModel.Cena = deo.Cena.ToString();
            deoViewModel.Velicina= deo.Velicina;
            deoViewModel.BrandId = deo.Brand.BrandId;
            deoViewModel.BrandName = deo.Brand.Name;

            if (deo.DeoImage != null)
            {
                deoViewModel.DeoImageViewModel = new DeoImageViewModel();
                deoViewModel.DeoImageViewModel.ImageData = deo.DeoImage.ImageData;
                deoViewModel.DeoImageViewModel.ImageMimeType = deo.DeoImage.ImageMimeType;
            }

            if (deo.DeoImages != null)
            {
                deoViewModel.DeoImageViewModels = new List<DeoImageViewModel>();
                foreach (var deoImage in deo.DeoImages)
                {
                    deoViewModel.DeoImageViewModels.Add(new DeoImageViewModel() { ImageData = deoImage.ImageData, ImageMimeType = deoImage.ImageMimeType });
                }
            }

            return deoViewModel;
        }
    }
}
