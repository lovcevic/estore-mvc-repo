﻿using Softversko2.EStore.Model.Entities.Users;
using Softversko2.EStore.Web.Models.Users;
using System.Collections.Generic;

namespace Softversko2.EStore.Web.Mappings.Users
{
    public static class UserMapper
    {
        public static List<UserViewModel> ConvertToUserViewModelList(this List<User> users)
        {
            List<UserViewModel> userViewModels = new List<UserViewModel>();
            foreach (User user in users)
            {
                userViewModels.Add(user.ConvertToUserViewModel());
            }
            return userViewModels;
        }

        public static UserViewModel ConvertToUserViewModel(this User user)
        {
            UserViewModel userViewModel = new UserViewModel();
            userViewModel.UserId = user.UserId;
            userViewModel.Username = user.Username;
            userViewModel.Email = user.Email;
            userViewModel.Password = user.Password;
            userViewModel.Role = user.Role;

            return userViewModel;
        }
    }
}
