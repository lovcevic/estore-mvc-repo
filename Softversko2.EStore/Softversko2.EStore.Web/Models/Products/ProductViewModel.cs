﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Softversko2.EStore.Web.Models.Products
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }
        [Required(ErrorMessage = "Ime opreme mora biti uneto")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Opis opreme mora biti unet")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Cena opreme mora biti uneta")]
        public string Price { get; set; }
        [Required(ErrorMessage = "Boja opreme mora biti uneta")]
        public string Color { get; set; }
        [Required(ErrorMessage = "Veličina opreme mora biti uneta")]
        public string Size { get; set; }
        [Required(ErrorMessage = "Marka mora biti unet")]
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        [Required(ErrorMessage = "Kategorija opreme mora biti uneta")]
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public ProductImageViewModel ProductImageViewModel { get; set; }
        public List<ProductImageViewModel> ProductImageViewModels { get; set; }
    }
}
