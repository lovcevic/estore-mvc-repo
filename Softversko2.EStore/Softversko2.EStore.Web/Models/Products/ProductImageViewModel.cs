﻿namespace Softversko2.EStore.Web.Models.Products
{
    public class ProductImageViewModel
    {
        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }
    }
}
