﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Softversko2.EStore.Web.Models.Delovi
{
    public class DeoImageViewModel
    {
        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }
    
}
}