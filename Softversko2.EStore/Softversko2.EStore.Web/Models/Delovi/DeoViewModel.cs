﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Softversko2.EStore.Web.Models.Delovi
{
    public class DeoViewModel
    {
        public int DeoId { get; set; }
        public string Ime { get; set; }
        [Required(ErrorMessage = "Ime  mora biti unet")]
        public string Opis { get; set; }
        [Required(ErrorMessage = "Opis  mora biti uneta")]
        public string Cena { get; set; }
        [Required(ErrorMessage = "Cena  mora biti uneta")]
        public string Velicina { get; set; }
        [Required(ErrorMessage = "Veličina mora biti unet")]
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        [Required(ErrorMessage = "Marka opreme mora biti uneta")]

        public DeoImageViewModel DeoImageViewModel { get; set; }
        public List<DeoImageViewModel> DeoImageViewModels { get; set; }
    }
}