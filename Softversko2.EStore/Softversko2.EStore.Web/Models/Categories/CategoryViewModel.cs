﻿using System.ComponentModel.DataAnnotations;

namespace Softversko2.EStore.Web.Models.Categories
{
    public class CategoryViewModel
    {
        public int CategoryId { get; set; }
        [Required(ErrorMessage = "Ime kategorije opreme mora biti uneto")]
        [StringLength(50, ErrorMessage = "Ime mora biti manje od 50 karaktera")]
        public string Name { get; set; }
    }
}
