﻿using Softversko2.EStore.Model.Entities.Users;
using System;
using System.ComponentModel.DataAnnotations;

namespace Softversko2.EStore.Web.Models.Users
{
    public class UserViewModel
    {
        public Guid UserId { get; set; }
        [Required(ErrorMessage = "Korisničko ime mora biti uneto")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Lozinka mora biti uneta")]
        public string Password { get; set; }
        [Required(ErrorMessage = "E-mail adresa mora biti uneta")]
        public string Email { get; set; }
        public Role Role { get; set; }
    }
}
