﻿using System.ComponentModel.DataAnnotations;

namespace Softversko2.EStore.Web.Models.Brands
{
    public class BrandViewModel
    {
        public int BrandId { get; set; }
        [Required(ErrorMessage = "Ime marke mora biti uneto")]
        [StringLength(50, ErrorMessage = "Ime mora biti manje od 50 karaktera")]
        public string Name { get; set; }
    }
}
