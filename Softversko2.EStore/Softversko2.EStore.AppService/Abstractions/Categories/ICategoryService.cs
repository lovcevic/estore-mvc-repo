﻿using Softversko2.EStore.AppService.Messages.Categories;

namespace Softversko2.EStore.AppService.Abstractions.Categories
{
    public interface ICategoryService
    {
        CreateCategoryResponse CreateCategory(CreateCategoryRequest request);
        FindAllCategoriesResponse FindAllCategories();
        UpdateCategoryResponse UpdateCategory(UpdateCategoryRequest request);
        DeleteCategoryResponse DeleteCategory(DeleteCategoryRequest request);

    }
}
