﻿using Softversko2.EStore.AppService.Messages.Users;

namespace Softversko2.EStore.AppService.Abstractions.Users
{
    public interface IUserService
    {
        CreateUserResponse CreateUser(CreateUserRequest request);
        FindAllUsersResponse ReadUsers();
        UpdateUserResponse UpdateUser(UpdateUserRequest request);
        DeleteUserResponse DeleteUser(DeleteUserRequest request);
        AuthenticateUserResponse AuthenticateUser(AuthenticateUserRequest request);
    }
}
