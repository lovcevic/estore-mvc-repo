﻿using Softversko2.EStore.AppService.Messages.Brands;

namespace Softversko2.EStore.AppService.Abstractions.Brands
{
    public interface IBrandService
    {
        CreateBrandResponse CreateBrand(CreateBrandRequest request);
        FindAllBrandsResponse FindAllBrands();
        UpdateBrandResponse UpdateBrand(UpdateBrandRequest request);
        DeleteBrandResponse DeleteBrand(DeleteBrandRequest request);
    }
}
