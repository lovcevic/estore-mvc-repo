﻿using Softversko2.EStore.AppService.Messages.Products;

namespace Softversko2.EStore.AppService.Abstractions.Products
{
    public interface IProductService
    {
        FindAllProductsResponse FindAllProducts();
        CreateProductResponse CreateProduct(CreateProductRequest request);
        UpdateProductResponse UpdateProduct(UpdateProductRequest request);
        DeleteProductResponse DeleteProduct(DeleteProductRequest request);
    }
}
