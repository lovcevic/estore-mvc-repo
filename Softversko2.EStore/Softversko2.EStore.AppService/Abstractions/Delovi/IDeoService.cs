﻿using Softversko2.EStore.AppService.Messages.Delovi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softversko2.EStore.AppService.Abstractions.Delovi
{
    public interface IDeoService
    {
        FindAllDeloviResponse FindAllDelovi();
        CreateDeloviResponse CreateDeo(CreateDeloviRequest request);
        UpdateDeloviResponse UpdateDeo(UpdateDeloviRequest request);
        DeleteDeloviResponse DeleteDeo(DeleteDeloviRequest request);
    }
}
