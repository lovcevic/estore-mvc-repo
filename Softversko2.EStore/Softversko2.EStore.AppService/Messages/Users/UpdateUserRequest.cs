﻿using System;

namespace Softversko2.EStore.AppService.Messages.Users
{
    public class UpdateUserRequest
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }
}
