﻿using Softversko2.EStore.AppService.Messages.Abstractions;

namespace Softversko2.EStore.AppService.Messages.Users
{
    public class AuthenticateUserResponse : ResponseBase
    {
        public bool Authenticated { get; set; }
    }
}
