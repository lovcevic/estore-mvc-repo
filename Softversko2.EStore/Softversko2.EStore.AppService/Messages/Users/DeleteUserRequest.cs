﻿using System;

namespace Softversko2.EStore.AppService.Messages.Users
{
    public class DeleteUserRequest
    {
        public Guid UserId { get; set; }
    }
}
