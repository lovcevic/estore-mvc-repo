﻿using Softversko2.EStore.AppService.Messages.Abstractions;
using Softversko2.EStore.Model.Entities.Users;
using System.Collections.Generic;

namespace Softversko2.EStore.AppService.Messages.Users
{
    public class FindAllUsersResponse : ResponseBase
    {
        public List<User> Users { get; set; }
    }
}
