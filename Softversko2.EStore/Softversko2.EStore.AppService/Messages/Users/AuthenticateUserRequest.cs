﻿namespace Softversko2.EStore.AppService.Messages.Users
{
    public class AuthenticateUserRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
