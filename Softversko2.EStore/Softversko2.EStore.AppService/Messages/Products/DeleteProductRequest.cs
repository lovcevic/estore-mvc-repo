﻿namespace Softversko2.EStore.AppService.Messages.Products
{
    public class DeleteProductRequest
    {
        public int ProductId { get; set; }
    }
}
