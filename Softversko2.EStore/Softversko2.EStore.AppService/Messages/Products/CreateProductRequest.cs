﻿using Softversko2.EStore.Model.Entities.Products;
using System.Collections.Generic;

namespace Softversko2.EStore.AppService.Messages.Products
{
    public class CreateProductRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public int BrandId { get; set; }
        public int CategoryId { get; set; }
        public ProductImage ProductImage { get; set; }
        public List<ProductImage> ProductImages { get; set; }
    }
}
