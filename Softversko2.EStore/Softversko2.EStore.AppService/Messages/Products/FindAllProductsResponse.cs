﻿using Softversko2.EStore.AppService.Messages.Abstractions;
using Softversko2.EStore.Model.Entities.Products;
using System.Collections.Generic;

namespace Softversko2.EStore.AppService.Messages.Products
{
    public class FindAllProductsResponse : ResponseBase
    {
        public List<Product> Products { get; set; }
    }
}
