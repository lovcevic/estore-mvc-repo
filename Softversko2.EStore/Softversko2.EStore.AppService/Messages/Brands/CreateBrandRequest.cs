﻿namespace Softversko2.EStore.AppService.Messages.Brands
{
    public class CreateBrandRequest
    {
        public string Name { get; set; }
    }
}
