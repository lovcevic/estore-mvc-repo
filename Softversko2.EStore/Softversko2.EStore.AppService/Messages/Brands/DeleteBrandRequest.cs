﻿namespace Softversko2.EStore.AppService.Messages.Brands
{
    public class DeleteBrandRequest
    {
        public int BrandId { get; set; }
    }
}
