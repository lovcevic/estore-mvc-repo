﻿using Softversko2.EStore.AppService.Messages.Abstractions;
using Softversko2.EStore.Model.Entities.Brands;
using System.Collections.Generic;

namespace Softversko2.EStore.AppService.Messages.Brands
{
    public class FindAllBrandsResponse : ResponseBase
    {
        public List<Brand> Brands { get; set; }
    }
}
