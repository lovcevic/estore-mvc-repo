﻿namespace Softversko2.EStore.AppService.Messages.Brands
{
    public class UpdateBrandRequest
    {
        public int BrandId { get; set; }
        public string Name { get; set; }
    }
}
