﻿namespace Softversko2.EStore.AppService.Messages.Categories
{
    public class DeleteCategoryRequest
    {
        public int CategoryId { get; set; }
    }
}
