﻿using Softversko2.EStore.AppService.Messages.Abstractions;
using Softversko2.EStore.Model.Entities.Categories;
using System.Collections.Generic;

namespace Softversko2.EStore.AppService.Messages.Categories
{
    public class FindAllCategoriesResponse : ResponseBase
    {
        public List<Category> Categories { get; set; }
    }
}
