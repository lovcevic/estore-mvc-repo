﻿namespace Softversko2.EStore.AppService.Messages.Categories
{
    public class CreateCategoryRequest
    {
        public string Name { get; set; }
    }
}
