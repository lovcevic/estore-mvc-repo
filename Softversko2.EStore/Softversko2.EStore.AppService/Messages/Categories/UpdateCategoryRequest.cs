﻿namespace Softversko2.EStore.AppService.Messages.Categories
{
    public class UpdateCategoryRequest
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
    }
}
