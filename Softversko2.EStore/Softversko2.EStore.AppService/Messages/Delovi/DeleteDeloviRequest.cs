﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softversko2.EStore.AppService.Messages.Delovi
{
   public class DeleteDeloviRequest
    {
        public int DeoId { get; set; }
    }
}
