﻿using Softversko2.EStore.Model.Entities.Delovi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softversko2.EStore.AppService.Messages.Delovi
{
  public  class CreateDeloviRequest
    {
        public string Ime { get; set; }
        public string Opis { get; set; }
        public string Cena { get; set; }
        public string Velicina { get; set; }
        public int BrandId { get; set; }
        public DeoImage DeoImage { get; set; }
        public List<DeoImage> DeoImages { get; set; }
    }
}
