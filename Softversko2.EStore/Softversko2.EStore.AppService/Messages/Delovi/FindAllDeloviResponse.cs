﻿using Softversko2.EStore.AppService.Messages.Abstractions;
using Softversko2.EStore.Model.Entities.Delovi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softversko2.EStore.AppService.Messages.Delovi
{
  public  class FindAllDeloviResponse: ResponseBase
    {
        public List<Deo> Delovi { get; set; }
    }
}
