﻿namespace Softversko2.EStore.AppService.Messages.Abstractions
{
    public abstract class ResponseBase
    {
        public string Message { get; set; }
        public bool Success { get; set; }
    }
}
