﻿using Softversko2.EStore.AppService.Messages.Delovi;
using Softversko2.EStore.Model.Entities.Brands;
using Softversko2.EStore.Model.Entities.Delovi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softversko2.EStore.AppService.Mappings.Delovi
{
   public static class DeoMapper
    { 
     public static Deo ConvertToDeo(this CreateDeloviRequest createRequest)
    {
        Deo deo= new Deo();
            deo.Ime = createRequest.Ime;
            deo.Opis = createRequest.Opis;
            deo.Cena = Decimal.Parse(createRequest.Cena);
            deo.Velicina = createRequest.Velicina;
            deo.Brand = new Brand() { BrandId = createRequest.BrandId };
            deo.DeoImage = createRequest.DeoImage;
            deo.DeoImages = createRequest.DeoImages;

        return deo;
    }

    public static Deo ConvertToDeo(this UpdateDeloviRequest updateRequest)
    {
        Deo deo = new Deo();
            deo.DeoId = updateRequest.DeoId;
            deo.Ime = updateRequest.Ime;
            deo.Opis = updateRequest.Opis;
            deo.Cena = Decimal.Parse(updateRequest.Cena);
            deo.Velicina = updateRequest.Velicina;
            deo.Brand = new Brand() { BrandId = updateRequest.BrandId };
            deo.DeoImage = updateRequest.DeoImage;
            deo.DeoImages = updateRequest.DeoImages;

        return deo;
    }
}
}
