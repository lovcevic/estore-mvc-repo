﻿using Softversko2.EStore.AppService.Abstractions.Delovi;
using Softversko2.EStore.AppService.Mappings.Delovi;
using Softversko2.EStore.AppService.Messages.Delovi;
using Softversko2.EStore.Model.Entities.Delovi;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softversko2.EStore.AppService.Implementations.Delovi
{
    public class DeoService : IDeoService
    {
        private IDeoRepository deoRepository;

        public DeoService(IDeoRepository deoRepository)
        {
            this.deoRepository = deoRepository;
        }

        public CreateDeloviResponse CreateDeo(CreateDeloviRequest request)
        {
            CreateDeloviResponse response = new CreateDeloviResponse();
            try
            {

               Deo deo= request.ConvertToDeo();
                deoRepository.Create(deo);
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public DeleteDeloviResponse DeleteDeo(DeleteDeloviRequest request)
        {
            DeleteDeloviResponse response = new DeleteDeloviResponse();
            try
            {
            deoRepository.Delete(request.DeoId);
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public UpdateDeloviResponse UpdateDeo(UpdateDeloviRequest request)
        {
            UpdateDeloviResponse response = new UpdateDeloviResponse();
            try
            {
                Deo deo= request.ConvertToDeo();
               deoRepository.Update(deo);
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public FindAllDeloviResponse FindAllDelovi()
        {
            FindAllDeloviResponse response = new FindAllDeloviResponse();
            try
            {
                response.Delovi =deoRepository.ReadAll();
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}


