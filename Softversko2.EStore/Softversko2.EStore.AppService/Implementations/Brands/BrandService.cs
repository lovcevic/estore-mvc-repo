﻿using Softversko2.EStore.AppService.Abstractions.Brands;
using Softversko2.EStore.AppService.Mappings.Brands;
using Softversko2.EStore.AppService.Messages.Brands;
using Softversko2.EStore.Model.Entities.Brands;
using System;

namespace Softversko2.EStore.AppService.Implementations.Brands
{
    public class BrandService : IBrandService
    {
        private IBrandRepository brandRepository;

        public BrandService(IBrandRepository brandRepository)
        {
            this.brandRepository = brandRepository;
        }

        public CreateBrandResponse CreateBrand(CreateBrandRequest request)
        {
            CreateBrandResponse response = new CreateBrandResponse();
            try
            {
                Brand brand = request.ConvertToBrand();
                brandRepository.Create(brand);
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public DeleteBrandResponse DeleteBrand(DeleteBrandRequest request)
        {
            DeleteBrandResponse response = new DeleteBrandResponse();
            try
            {
                brandRepository.Delete(request.BrandId);
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public FindAllBrandsResponse FindAllBrands()
        {
            FindAllBrandsResponse response = new FindAllBrandsResponse();
            try
            {
                response.Brands = brandRepository.ReadAll();
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public UpdateBrandResponse UpdateBrand(UpdateBrandRequest request)
        {
            UpdateBrandResponse response = new UpdateBrandResponse();
            try
            {
                Brand brand = request.ConvertToBrand();
                brandRepository.Update(brand);
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}
