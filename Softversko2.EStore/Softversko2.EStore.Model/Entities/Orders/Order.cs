﻿using Softversko2.EStore.Model.Entities.Carts;
using Softversko2.EStore.Model.Entities.Shipping;

namespace Softversko2.EStore.Model.Entities.Orders
{
    public class Order
    {
        public Cart Cart { get; set; }
        public ShippingDetails ShippingDetails { get; set; }
        public IDeliveryOperator DeliveryOperator { get; set; }
    }
}
