﻿namespace Softversko2.EStore.Model.Entities.Carts
{
    public interface ICartDiscountStrategy
    {
        decimal GetTotalCostAfterApplyingDiscountTo(Cart cart);
    }
}
