﻿namespace Softversko2.EStore.Model.Entities.Carts
{
    public class CartDiscountMoneyOff : ICartDiscountStrategy
    {
        public decimal GetTotalCostAfterApplyingDiscountTo(Cart cart)
        {
            if (cart.ComputeTotalValue() > 5000)
                return cart.ComputeTotalValue() - 500m;
            if (cart.ComputeTotalValue() > 2000)
                return cart.ComputeTotalValue() - 200m;
            else
                return cart.ComputeTotalValue();
        }
    }
}
