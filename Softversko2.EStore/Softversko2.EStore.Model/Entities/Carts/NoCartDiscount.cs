﻿namespace Softversko2.EStore.Model.Entities.Carts
{
    public class NoCartDiscount : ICartDiscountStrategy
    {
        public decimal GetTotalCostAfterApplyingDiscountTo(Cart cart)
        {
            return cart.ComputeTotalValue();
        }
    }
}
