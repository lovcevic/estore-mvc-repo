﻿using System.Collections.Generic;

namespace Softversko2.EStore.Model.Entities.Categories
{
    public interface ICategoryRepository
    {
        List<Category> ReadAll();
        void Update(Category entity);
        void Create(Category entity);
        void Delete(int id);
    }
}
