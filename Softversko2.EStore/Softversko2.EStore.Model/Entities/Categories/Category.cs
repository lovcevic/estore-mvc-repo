﻿namespace Softversko2.EStore.Model.Entities.Categories
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
    }
}
