﻿using System;
using System.Collections.Generic;

namespace Softversko2.EStore.Model.Entities.Users
{
    public interface IUserRepository
    {
        List<User> ReadAll();
        void Update(User entity);
        void Create(User entity);
        void Delete(Guid id);
    }
}
