﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softversko2.EStore.Model.Entities.Delovi
{
   public class DeoImage
    {
        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }
    }
}
