﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softversko2.EStore.Model.Entities.Delovi
{
   public interface IDeoRepository
    {
        List<Deo> ReadAll();
        void Update(Deo entity);
        void Create(Deo entity);
        void Delete(int id);
    }
}
