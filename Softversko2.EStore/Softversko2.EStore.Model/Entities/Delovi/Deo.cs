﻿using Softversko2.EStore.Model.Entities.Brands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softversko2.EStore.Model.Entities.Delovi
{
   public class Deo
    {
        public int DeoId { get; set; }
        public string Ime { get; set; }
        public string Opis { get; set; }

        public string Velicina { get; set; }
        public decimal Cena { get; set; }
       

        public Brand Brand { get; set; }

        public DeoImage DeoImage { get; set; }
        public List<DeoImage> DeoImages { get; set; }
    }
}
