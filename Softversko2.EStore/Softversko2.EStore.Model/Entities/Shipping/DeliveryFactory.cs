﻿namespace Softversko2.EStore.Model.Entities.Shipping
{
    public static class DeliveryFactory
    {
        public static IDeliveryOperator CreateDeliveryOperator(string operatorName)
        {
            if (operatorName == "Aks")
                return new AksDelivery();
            else
                return new BexDelivery();
        }
    }
}
