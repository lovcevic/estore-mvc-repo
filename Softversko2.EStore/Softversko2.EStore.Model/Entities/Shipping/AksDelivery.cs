﻿namespace Softversko2.EStore.Model.Entities.Shipping
{
    public class AksDelivery : IDeliveryOperator
    {
        public decimal getDeliveryPrice()
        {
            return 250;
        }

        public string getDeliveryTime()
        {
            return "3 radna dana";
        }
    }
}
