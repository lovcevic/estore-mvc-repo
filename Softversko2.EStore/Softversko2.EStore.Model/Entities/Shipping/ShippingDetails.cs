﻿namespace Softversko2.EStore.Model.Entities.Shipping
{
    public class ShippingDetails
    {
        public string Address { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
    }
}
