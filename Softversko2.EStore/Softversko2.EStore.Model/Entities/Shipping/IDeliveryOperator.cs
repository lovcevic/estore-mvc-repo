﻿namespace Softversko2.EStore.Model.Entities.Shipping
{
    public interface IDeliveryOperator
    {
        decimal getDeliveryPrice();
        string getDeliveryTime();
    }
}
