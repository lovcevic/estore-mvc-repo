﻿namespace Softversko2.EStore.Model.Entities.Shipping
{
    public class BexDelivery : IDeliveryOperator
    {
        public decimal getDeliveryPrice()
        {
            return 300M;
        }

        public string getDeliveryTime()
        {
            return "2 radna dana";
        }
    }
}
