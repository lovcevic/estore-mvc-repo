﻿namespace Softversko2.EStore.Model.Entities.Brands
{
    public class Brand
    {
        public int BrandId { get; set; }
        public string Name { get; set; }
    }
}
