﻿using System.Collections.Generic;

namespace Softversko2.EStore.Model.Entities.Brands
{
    public interface IBrandRepository
    {
        List<Brand> ReadAll();
        void Update(Brand entity);
        void Create(Brand entity);
        void Delete(int id);
    }
}
