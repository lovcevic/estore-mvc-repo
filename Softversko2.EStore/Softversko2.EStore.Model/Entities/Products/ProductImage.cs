﻿namespace Softversko2.EStore.Model.Entities.Products
{
    public class ProductImage
    {
        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }
    }
}
