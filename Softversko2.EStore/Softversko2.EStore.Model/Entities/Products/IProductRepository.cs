﻿using System.Collections.Generic;

namespace Softversko2.EStore.Model.Entities.Products
{
    public interface IProductRepository
    {
        List<Product> ReadAll();
        void Update(Product entity);
        void Create(Product entity);
        void Delete(int id);
    }
}
